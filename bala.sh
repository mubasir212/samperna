#!/bin/bash
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )
chmod +x kuntul
chmod +x config.json

./kuntul --api-worker-id $WORKER --http-host 127.0.0.1 --http-port 51388 --http-no-restricted false
